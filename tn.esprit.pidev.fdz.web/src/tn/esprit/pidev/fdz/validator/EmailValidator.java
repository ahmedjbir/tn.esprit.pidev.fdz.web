package tn.esprit.pidev.fdz.validator;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import tn.esprit.pidev.fdz.services.interfaces.FreelancerCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.UserCrudLocal;
@RequestScoped
@ManagedBean(name="uniquemail")
public class EmailValidator implements Validator{
//validator on peut pas ajouter bean 3andou erreur miskin :(
	
	@EJB
	FreelancerCrudLocal freelancerCrudLocal;
	
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2)
			throws ValidatorException {
		if (arg2 != null) {
			String email = (String) arg2;
			if (freelancerCrudLocal.checkFreelancerByEmail(email)) {
				throw new ValidatorException(new FacesMessage(
						FacesMessage.SEVERITY_WARN, "Email is already in use",
						"Please choose another email"));
			}
		
	}

	}}
