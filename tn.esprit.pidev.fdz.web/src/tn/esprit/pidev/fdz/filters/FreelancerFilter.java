package tn.esprit.pidev.fdz.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tn.esprit.pidev.fdz.managedbeans.AuthenticationBean;


@WebFilter(filterName = "freelancerFilter", urlPatterns = { "/pages/freelancer/*" })
public class FreelancerFilter implements Filter{
	
	

	public FreelancerFilter() {

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		AuthenticationBean authentication = (AuthenticationBean) req.getSession().getAttribute("authentication");

		try {
			if (authentication.isLogged()) {
				
					chain.doFilter(req, res);
				
			} else {
				res.sendRedirect(req.getContextPath() + "/pages/public/login.jsf");
			}
		} catch (NullPointerException e) {
			res.sendRedirect(req.getContextPath() + "/pages/public/login.jsf");
		}

	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
