package tn.esprit.pidev.fdz.managedbeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import tn.esprit.pidev.fdz.entities.Job;
import tn.esprit.pidev.fdz.entities.User;
import tn.esprit.pidev.fdz.services.interfaces.JobCrudLocal;

@ViewScoped
@ManagedBean
public class JobsBean {
	
	@EJB
	private JobCrudLocal jc;
	
	@ManagedProperty(value = "#{authentication}")
	private AuthenticationBean authenticationBean;
	
	private List<Job> jobs;
	private List<Job> startedJobs;
	private List<Job> completedJobs;
	private List<Job> allWithoutFreelancerJobs;
	private List<Job> filteredJobs;
	private User freelancer;
	
	@PostConstruct
	private void init() {
		freelancer = authenticationBean.getFreelancer();
		jobs = jc.findAll();
		startedJobs = jc.allStarted(freelancer,"0");
		completedJobs = jc.allStarted(freelancer, "1");
		allWithoutFreelancerJobs = jc.allWithoutFreelancer();
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	public List<Job> getStartedJobs() {
		return startedJobs;
	}

	public void setStartedJobs(List<Job> startedJobs) {
		this.startedJobs = startedJobs;
	}	
	
	public List<Job> getCompletedJobs() {
		return completedJobs;
	}

	public void setCompletedJobs(List<Job> completedJobs) {
		this.completedJobs = completedJobs;
	}
	
	public List<Job> getAllWithoutFreelancerJobs() {
		return allWithoutFreelancerJobs;
	}

	public void setAllWithoutFreelancerJobs(List<Job> allWithoutFreelancerJobs) {
		this.allWithoutFreelancerJobs = allWithoutFreelancerJobs;
	}

	
	public List<Job> getFilteredJobs() {
		return filteredJobs;
	}

	public void setFilteredJobs(List<Job> filteredJobs) {
		this.filteredJobs = filteredJobs;
	}

	public User getFreelancer() {
		return freelancer;
	}

	public void setFreelancer(User freelancer) {
		this.freelancer = freelancer;
	}
	
	public AuthenticationBean getAuthenticationBean() {
		return authenticationBean;
	}

	public void setAuthenticationBean(AuthenticationBean authenticationBean) {
		this.authenticationBean = authenticationBean;
	}
	
	


}
