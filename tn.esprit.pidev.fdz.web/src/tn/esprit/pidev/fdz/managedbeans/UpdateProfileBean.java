package tn.esprit.pidev.fdz.managedbeans;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import tn.esprit.pidev.fdz.entities.Freelancer;
import tn.esprit.pidev.fdz.services.interfaces.FreelancerCrudLocal;

@ViewScoped
@ManagedBean(name = "UpdateProfile")
public class UpdateProfileBean {
	
	
	@EJB
	 private FreelancerCrudLocal freelancerCrudLocal;
	
	@ManagedProperty(value = "#{authentication}")
	private AuthenticationBean authenticationBean;
	
	private Freelancer freelancer;
	
	
	@PostConstruct
	private void init() {
		
		
	freelancer = freelancerCrudLocal.findByLoginAndPassword(authenticationBean.getLogin(), authenticationBean.getPassword());
	
		
	
	}
   
	
	public String doUpdateProfile() {
		freelancerCrudLocal.edit(freelancer);;
		authenticationBean.setLogin(freelancer.getLogin());
		authenticationBean.setPassword(freelancer.getPassword());
		return "index?faces-redirect=true";
	}


	public AuthenticationBean getAuthenticationBean() {
		return authenticationBean;
	}


	public void setAuthenticationBean(AuthenticationBean authenticationBean) {
		this.authenticationBean = authenticationBean;
	}


	public Freelancer getFreelancer() {
		return freelancer;
	}


	public void setFreelancer(Freelancer freelancer) {
		this.freelancer = freelancer;
	}
	
	

}
