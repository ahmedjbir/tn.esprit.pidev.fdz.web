package tn.esprit.pidev.fdz.managedbeans;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;




import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import tn.esprit.pidev.fdz.entities.Freelancer;
import tn.esprit.pidev.fdz.services.interfaces.CategoryCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.FreelancerCrudLocal;


@ViewScoped
@ManagedBean(name = "registration")
public class RegistrationBean {
	
	private String destination = System.getProperty("jboss.home.dir")
			+ "\\welcome-content\\tmp\\";
	
	
	@EJB
	private FreelancerCrudLocal freelancerCrud;
	
	@EJB
	private CategoryCrudLocal categoryCrudLocal;
	
	private String file;
	private String firstname;
	private String lastname;
	private String email;
	private String login;
	private String password;
	private int cin;
	private String city;
	private String country;
	private String photo;
	private String companyname;
	private String cv;
	private Freelancer freelancer;
	private String host;
	//private List<Category categories ;
	
	
	//categories= categoryCrudLocal.ListCategory();
	
	
	@PostConstruct
	public void init(){
		setDestination(System.getProperty("jboss.home.dir")
				+ "\\welcome-content\\tmp\\");
		System.out.println(getDestination());

		HttpServletRequest req = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		host = req.getHeader("host");

		setDestination(System.getProperty("jboss.home.dir")
				+ "\\welcome-content\\tmp\\");
		
		
		
	}
	
	public String doRegister() {
		//login, password, firstname, lastname, email, city, country, photo,cv 
		freelancer = new Freelancer();
		freelancer.setLogin(login);
		freelancer.setPassword(password);
		freelancer.setFirstName(firstname);
		freelancer.setLastName(lastname);
		freelancer.setEmail(email);
		freelancer.setCity(city);
		freelancer.setCountry(country);
		freelancer.setPhoto(photo);
		
		
		//if(file != null) {
        //    FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
        //    FacesContext.getCurrentInstance().addMessage(null, message);
         // cv=file.getFileName().toString();
           
          //  cv=file.getFileName().toString();
            
            
            freelancer.setCv(file);
         			freelancerCrud.create(freelancer);
     //   }
		 
			
		
			return "login?faces-redirect=true";
		
	}


	public void handleFileUpload(FileUploadEvent event) {

		// FacesMessage msg = new FacesMessage(
		// FacesMessage.SEVERITY_INFO,"Success! ", event.getFile()
		// .getFileName() + " is uploaded.");
		// FacesContext.getCurrentInstance().addMessage(null, msg);
		// Do what you want with the file
		try {
			copyFile(event.getFile().getFileName(), event.getFile()
					.getInputstream());
			
			file = event.getFile().getFileName();
//			freelancer.setCv(file);
//			freelancerCrud.edit(freelancer);
			System.out.println("blaba");
			

		} catch (IOException e) {
			file = null;

			e.printStackTrace();
		}

	}

	public void copyFile(String file, InputStream in) {
	//	System.out.println(destination);
		//System.out.println(file);
		try {

			// write the inputStream to a FileOutputStream
			OutputStream out = new FileOutputStream(new File(destination
					+ file));

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = in.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}

			in.close();
			out.flush();
			out.close();

			
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	
	}
	     
	   

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getFile() {
		return file;
	}


	public void setFile(String file) {
		this.file = file;
	}


	public String getDestination() {
		return destination;
	}


	public void setDestination(String destination) {
		this.destination = destination;
	}


	public String getFirstname() {
		return firstname;
	}




	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}




	public String getLastname() {
		return lastname;
	}




	public void setLastname(String lastname) {
		this.lastname = lastname;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}




	public String getLogin() {
		return login;
	}




	public void setLogin(String login) {
		this.login = login;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public int getCin() {
		return cin;
	}




	public void setCin(int cin) {
		this.cin = cin;
	}




	public String getCity() {
		return city;
	}




	public void setCity(String city) {
		this.city = city;
	}




	public String getCountry() {
		return country;
	}




	public void setCountry(String country) {
		this.country = country;
	}




	public String getPhoto() {
		return photo;
	}




	public void setPhoto(String photo) {
		this.photo = photo;
	}




	public String getCompanyname() {
		return companyname;
	}




	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}




	public String getCv() {
		return cv;
	}




	public void setCv(String cv) {
		this.cv = cv;
	}




	

}
