package tn.esprit.pidev.fdz.managedbeans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.pidev.fdz.entities.Freelancer;
import tn.esprit.pidev.fdz.services.interfaces.FreelancerCrudLocal;


@SessionScoped
@ManagedBean(name = "authentication")
public class AuthenticationBean {
	

	@EJB
	private FreelancerCrudLocal fc;

	private String login;
	private String password;
	private boolean keepMeSignedIn;
	private boolean logged;
	private Freelancer freelancer;

	public String doLogin() {
		
			freelancer = fc.findByLoginAndPassword(login, password);
			//System.out.println(freelancer.getLogin()+"...... "+freelancer.getPassword());
			if (freelancer != null)
			{
				logged = true;
				return "/pages/freelancer/index?faces-redirect=true";
			}
			else
			{
				return "login?faces-redirect=true";
			}
		//	return "/pages/freelancer/index?faces-redirect=true";

	}

	public String doLogout() {
		FacesContext.getCurrentInstance().getExternalContext()
				.invalidateSession();
		return "/pages/public/login?faces-redirect=true";
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isKeepMeSignedIn() {
		return keepMeSignedIn;
	}

	public void setKeepMeSignedIn(boolean keepMeSignedIn) {
		this.keepMeSignedIn = keepMeSignedIn;
	}

	public boolean isLogged() {
		return logged;
	}

	public void setLogged(boolean logged) {
		this.logged = logged;
	}

	public Freelancer getFreelancer() {
		return freelancer;
	}

	public void setFreelancer(Freelancer freelancer) {
		this.freelancer = freelancer;
	}		


}