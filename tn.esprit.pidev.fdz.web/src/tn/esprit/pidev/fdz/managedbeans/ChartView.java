package tn.esprit.pidev.fdz.managedbeans;

import javax.annotation.PostConstruct;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.primefaces.model.chart.PieChartModel;

import tn.esprit.pidev.fdz.entities.Job;
import tn.esprit.pidev.fdz.entities.User;
import tn.esprit.pidev.fdz.services.interfaces.JobCrudLocal;
 
@ManagedBean
public class ChartView implements Serializable { 

	@EJB
	private JobCrudLocal jc;
	
	@ManagedProperty(value = "#{authentication}")
	private AuthenticationBean authenticationBean;
	
    private PieChartModel pieModel;
    
    private User freelancer;
    private List<Job> startedJobs;
	private List<Job> completedJobs;
 
    @PostConstruct
    public void init() {
		freelancer = authenticationBean.getFreelancer();

		startedJobs = jc.allStarted(freelancer,"0");
		completedJobs = jc.allStarted(freelancer, "1");
        createPieModels();
    }

     
    public PieChartModel getPieModel() {
        return pieModel;
    }
     
    private void createPieModels() {
        createPieModel();
    }
 
     
    public AuthenticationBean getAuthenticationBean() {
		return authenticationBean;
	}


	public void setAuthenticationBean(AuthenticationBean authenticationBean) {
		this.authenticationBean = authenticationBean;
	}


	public User getFreelancer() {
		return freelancer;
	}


	public void setFreelancer(User freelancer) {
		this.freelancer = freelancer;
	}


	public List<Job> getStartedJobs() {
		return startedJobs;
	}


	public void setStartedJobs(List<Job> startedJobs) {
		this.startedJobs = startedJobs;
	}


	public List<Job> getCompletedJobs() {
		return completedJobs;
	}


	public void setCompletedJobs(List<Job> completedJobs) {
		this.completedJobs = completedJobs;
	}


	private void createPieModel() {
        pieModel = new PieChartModel();
         
        pieModel.set("Started", startedJobs.size());
        pieModel.set("Completed", completedJobs.size());
         
        pieModel.setTitle("Statistics for Your jobs");
        pieModel.setLegendPosition("e");
        pieModel.setFill(false);
        pieModel.setShowDataLabels(true);
        pieModel.setDiameter(150);
    }
     
}