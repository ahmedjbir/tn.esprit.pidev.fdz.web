package tn.esprit.pidev.fdz.managedbeans;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.FileUploadEvent;

import tn.esprit.pidev.fdz.entities.Job;
import tn.esprit.pidev.fdz.entities.JobOwner;
import tn.esprit.pidev.fdz.entities.User;
import tn.esprit.pidev.fdz.services.interfaces.JobCrudLocal;
import tn.esprit.pidev.fdz.services.interfaces.MailLocal;

@SessionScoped
@ManagedBean
public class DetailsJob {
	
	private String destination = System.getProperty("jboss.home.dir")
			+ "\\welcome-content\\tmp\\Upload\\";

	@EJB
	private JobCrudLocal jc;
	
	@EJB
	private MailLocal ml;
	
	@ManagedProperty(value = "#{authentication}")
	private AuthenticationBean authenticationBean;

	private String file;
	private String host;
	private Job job;
	Integer rate;
	private User freelancer;
	
	@PostConstruct
	private void init() {
		freelancer = authenticationBean.getFreelancer();
		
		setDestination(System.getProperty("jboss.home.dir")
				+ "\\welcome-content\\tmp\\Upload\\");
		System.out.println(getDestination());

		HttpServletRequest req = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		host = req.getHeader("host");

		setDestination(System.getProperty("jboss.home.dir")
				+ "\\welcome-content\\tmp\\Upload\\");
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}
	
	public String goDetailsJob(){
		return "/pages/freelancer/detailsJob?faces-redirect=true";
	}
	
	public String goDetailsJobCompleted(){
		return "/pages/freelancer/detailsJobCompleted?faces-redirect=true";
	}
	
	public String goDetailsJobToChoose(){
		return "/pages/freelancer/detailsJobToChoose?faces-redirect=true";
	}	
	
	public Integer getRate() {
		rate = (int) job.getRate();
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}
	
	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
	
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String doUpdateJob()
	{
		String Newligne=System.getProperty("line.separator"); 
		String content= "Dear "+job.getJobOwner().getFirstName() + Newligne + "Free Dev Zone inform you that your job is already completed."+Newligne+Newligne+"We invite you to join our Platform.";
		JobOwner jo =(JobOwner) job.getJobOwner();
		String to=jo.getEmail();
		
		if(job.getWorkLoad()==100)
		{
			job.setIsDone("1");
			jc.edit(job);
			ml.sendMail(to, "Job Completed", content);
			return "uploadJob?faces-redirect=true";
		}
		else
		{
		jc.edit(job);
		return "detailsJob?faces-redirect=true";
		}
	}
	
	
	// ALA : Function for the freelancer To choose the Job
	
	public String doUpdateJobToChoose()
	{
		job.setFreelancer(freelancer);
		
		jc.edit(job);
		return "index?faces-redirect=true";		
	}

	public AuthenticationBean getAuthenticationBean() {
		return authenticationBean;
	}

	public void setAuthenticationBean(AuthenticationBean authenticationBean) {
		this.authenticationBean = authenticationBean;
	}

	public User getFreelancer() {
		return freelancer;
	}

	public void setFreelancer(User freelancer) {
		this.freelancer = freelancer;
	}
	

	public void handleFileUpload(FileUploadEvent event) {

		try {
			copyFile(event.getFile().getFileName(), event.getFile()
					.getInputstream());
			
			file = event.getFile().getFileName();
			

		} catch (IOException e) {
			file = null;

			e.printStackTrace();
		}

	}

	public void copyFile(String file, InputStream in) {

		try {

			OutputStream out = new FileOutputStream(new File(destination
					+ file));

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = in.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}

			in.close();
			out.flush();
			out.close();
			
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}	
	}
	
	public String doUpload()
	{
		job.setUpDownload(file);		
		jc.edit(job);
		return "completedJobs?faces-redirect=true";
	}

}
